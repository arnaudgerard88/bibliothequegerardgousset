package fr.miage.gestion.empruntresa;

import fr.miage.entity.empruntresa.Emprunt;
import fr.miage.entity.empruntresa.Reservation;
import fr.miage.entity.livreusager.Usager;
import fr.miage.resources.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.*;

@Controller
@RequestMapping("/emprunt")
public class EmpruntController {

    @Autowired
    EmpruntResource emr;

    @Autowired
    ExemplaireResource exr;

    @Autowired
    ReservationResource rr;

    @Autowired
    OeuvreResource or;

    @Autowired
    UsagerResource ur;

    @GetMapping("/list")
    public String list(Model model) {
        model.addAttribute("module", "emprunt");
        model.addAttribute("emprunts", emr.findAll());
        return "emprunt/listeemprunt";
    }

    @GetMapping("/create")
    public String create(Model model, @RequestParam(name = "usager", required = false) String idUsager, @RequestParam(name = "oeuvre", required = false) String idOeuvre) {

        model.addAttribute("module", "emprunt");
        model.addAttribute("emprunt", new Emprunt());
        if(idUsager != null) {
            List<Usager> usagers = new ArrayList<>();
            usagers.add(ur.findById(idUsager).get());
            model.addAttribute("usagers", usagers);
        } else {
            model.addAttribute("usagers", ur.findAll());
        }
        if(idOeuvre != null){
            model.addAttribute("exemplaires", exr.findExemplaireDispoByIdOeuvre(or.findById(idOeuvre).get()));
        }else{
            model.addAttribute("exemplaires", exr.findExemplaireDispo());
        }
        model.addAttribute("oeuvres", or.findAll());
        return "emprunt/creeremprunt";
    }

    @PostMapping("/create")
    public String createPost(@Valid @ModelAttribute Emprunt emprunt, BindingResult result) {
        if(result.hasErrors()) {

        } else {
            emprunt.setId(UUID.randomUUID().toString());
            emprunt.setDatejour(new Date());
            emprunt.setDateretour(new Date(new Date().getTime()+Reservation.NB_JOURS_AVANT_RETOUR*24*60*60*1000));
            emprunt.getExemplaire().setDispo(false);
            emr.save(emprunt);
            exr.save(emprunt.getExemplaire());
            //VERIFICATION RESERVATION EXISTANTE
            List<Reservation> resa = rr.findReservationExistanteByIdOeuvreAndIdUsager(emprunt.getExemplaire().getOeuvre().getId(),emprunt.getUsager().getId());
            if(resa.size() > 0){
                rr.delete(resa.get(0));
            }
        }

        return "redirect:/emprunt/list";
    }

    @GetMapping("/rendre/{id}")
    public String rendre(@PathVariable String id, Model model) {
        model.addAttribute("module", "emprunt");
        model.addAttribute("emprunt", emr.findById(id).get());
        model.addAttribute("idEmprunt", id);
        return "emprunt/rendreemprunt";
    }

    @PostMapping("/rendre/{id}")
    public String rendrePost(@Valid @ModelAttribute Emprunt emprunt, BindingResult result) {
        emprunt.getExemplaire().setDispo(true);
        exr.save(emprunt.getExemplaire());
        emr.delete(emprunt);
        return "redirect:/emprunt/list";
    }
}
