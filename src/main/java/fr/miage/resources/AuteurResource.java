package fr.miage.resources;

import fr.miage.entity.livreusager.Auteur;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuteurResource extends JpaRepository<Auteur, String> {

}
