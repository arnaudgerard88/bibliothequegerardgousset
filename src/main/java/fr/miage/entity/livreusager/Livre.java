package fr.miage.entity.livreusager;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Set;

@Entity
@DiscriminatorValue("livre")
public class Livre extends Oeuvre {

    @OneToMany(mappedBy = "livre")
    //@JoinTable(name = "livreauteur", joinColumns = { @JoinColumn(name = "id_oeuvre", referencedColumnName = "id") }, inverseJoinColumns = { @JoinColumn(name = "id_auteur", referencedColumnName = "id") })
    private Set<Livreauteur> auteur;

    public Livre() {
        super();
    }

    public Set<Livreauteur> getAuteur() {
        return auteur;
    }

    public void setAuteur(Set<Livreauteur> auteur) {
        this.auteur = auteur;
    }
}
