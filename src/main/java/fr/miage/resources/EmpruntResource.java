package fr.miage.resources;

import fr.miage.entity.empruntresa.Emprunt;
import fr.miage.entity.livreusager.Exemplaire;
import fr.miage.entity.livreusager.Oeuvre;
import fr.miage.entity.livreusager.Usager;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface EmpruntResource extends JpaRepository<Emprunt, String> {

    @Query("select e from Emprunt e where e.exemplaire = ?1")
    List<Emprunt> findEmpruntByExemplaire(Exemplaire exemplaire);

    @Query("select e from Emprunt e where e.usager = ?1")
    List<Emprunt> findEmpruntByUsager(Usager usager);
}
