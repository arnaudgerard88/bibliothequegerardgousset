package fr.miage.resources;

import fr.miage.entity.empruntresa.Emprunt;
import fr.miage.entity.empruntresa.Reservation;
import fr.miage.entity.livreusager.Exemplaire;
import fr.miage.entity.livreusager.Usager;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ReservationResource extends JpaRepository<Reservation, String> {

    @Query(value = "select r from Reservation r where r.usager.id = :#{#idUsager} and r.oeuvre.id = :#{#idOeuvre}")
    List<Reservation> findReservationExistanteByIdOeuvreAndIdUsager(@Param("idOeuvre") String idOeuvre, @Param("idUsager") String idUsager);

    @Query("select r from Reservation r where r.usager = ?1")
    List<Reservation> findReservationByUsager(Usager usager);
}
