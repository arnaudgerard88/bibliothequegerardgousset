package fr.miage.gestion.livreusager;

import fr.miage.entity.enumerateur.Etat;
import fr.miage.entity.livreusager.Exemplaire;
import fr.miage.entity.livreusager.Oeuvre;
import fr.miage.resources.EmpruntResource;
import fr.miage.resources.ExemplaireResource;
import fr.miage.resources.OeuvreResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;

@Controller
@RequestMapping("/exemplaire")
public class ExemplaireController {

    @Autowired
    ExemplaireResource er;

    @Autowired
    EmpruntResource emr;

    @Autowired
    OeuvreResource or;

    @GetMapping("/list")
    public String list(Model model) {
        model.addAttribute("module", "exemplaire");
        model.addAttribute("exemplaires", er.findAll());
        return "exemplaire/listeexemplaire";
    }

    @GetMapping("/create")
    public String create(Model model) {
        model.addAttribute("module", "exemplaire");
        model.addAttribute("exemplaire", new Exemplaire());
        model.addAttribute("oeuvres", or.findAll());
        return "exemplaire/creerexemplaire";
    }

    @PostMapping("/create")
    public String create(@Valid @ModelAttribute Exemplaire exemplaire, BindingResult result) {
        if(result.hasErrors()) {

        } else {
            exemplaire.setId(UUID.randomUUID().toString());
            exemplaire.setDispo(true);
            exemplaire.setEtat(Etat.NEUF);
            exemplaire.getOeuvre().setStock(exemplaire.getOeuvre().getStock()+1);
            er.save(exemplaire);
            or.save(exemplaire.getOeuvre());
        }
        return "redirect:/exemplaire/list";
    }

    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, Model model) {
        Exemplaire exemplaire = er.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Id usager non valide:" + id));
        model.addAttribute("exemplaire", exemplaire);
        model.addAttribute("module", "exemplaire");
        return "exemplaire/modifierexemplaire";
    }

    @PostMapping("/edit/{id}")
    public String editPost(@PathVariable("id") String id, @Valid @ModelAttribute Exemplaire exemplaire, BindingResult result) {
        Exemplaire real = er.findById(exemplaire.getId()).get();
        exemplaire.setOeuvre(real.getOeuvre());
        exemplaire.setDispo(real.isDispo());
        er.save(exemplaire);
        return "redirect:/exemplaire/list";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable String id) {
        Exemplaire exemplaire = er.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Id exemplaire non valide: "+id));
        if(!emr.findEmpruntByExemplaire(exemplaire).isEmpty()) {
            return "exemplaire/erreurexemplaire";
        }
        Oeuvre oeuvre = exemplaire.getOeuvre();
        oeuvre.setStock(oeuvre.getStock() - 1);
        or.save(oeuvre);
        er.delete(exemplaire);
        return "redirect:/exemplaire/list";
    }
}
