package fr.miage.resources;

import fr.miage.entity.livreusager.Livreauteur;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LivreAuteurResource extends JpaRepository<Livreauteur, String> {
}
