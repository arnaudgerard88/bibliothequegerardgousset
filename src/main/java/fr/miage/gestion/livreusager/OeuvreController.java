package fr.miage.gestion.livreusager;

import fr.miage.entity.livreusager.*;
import fr.miage.resources.AuteurResource;
import fr.miage.resources.LivreAuteurResource;
import fr.miage.resources.LivreResource;
import fr.miage.resources.OeuvreResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

import static fr.miage.entity.enumerateur.Type.LIVRE;

@Controller
@RequestMapping("/oeuvre")
public class OeuvreController {

    @Autowired
    OeuvreResource or;

    @Autowired
    AuteurResource ar;

    @Autowired
    LivreAuteurResource lar;

    @Autowired
    LivreResource lr;

    @GetMapping("/list")
    public String list(Model model) {
        model.addAttribute("oeuvres", or.findAll());
        model.addAttribute("module", "oeuvre");
        return "oeuvre/listeoeuvre";
    }

    @GetMapping("/create")
    public String creer(Model model) {
        model.addAttribute("oeuvre", new Oeuvre());
        model.addAttribute("module", "oeuvre");
        model.addAttribute("auteurs", ar.findAll());
        return "oeuvre/creeroeuvre";
    }

    @PostMapping("/create")
    public String creerPost(@ModelAttribute Oeuvre oeuvre, @RequestParam("idauteur") String idAuteur) throws Exception {
        oeuvre.setId(UUID.randomUUID().toString());
        Oeuvre res = null;
        switch (oeuvre.getType()) {
            case LIVRE:
                res = new Livre();
                break;
            case MAGAZINE:
                res = new Magazine();
                break;
            default:
                throw new Exception("Type d'oeuvre inconnu");
        }
        res.setId(oeuvre.getId());
        res.setStock(0);
        res.setExemplaires(oeuvre.getExemplaires());
        res.setTitre(oeuvre.getTitre());
        res.setType(oeuvre.getType());
        or.save(res);
        if(oeuvre.getType() == LIVRE) {
            Livre livre = lr.findById(res.getId()).get();
            Auteur auteur = ar.findById(idAuteur).get();
            Livreauteur livreauteur = new Livreauteur(auteur, livre);
            livreauteur.setId(UUID.randomUUID().toString());
            lar.save(livreauteur);
        }
        return "redirect:/oeuvre/list";
    }
}
