package fr.miage.entity.enumerateur;

public enum Etat {
    NEUF("Neuf"),
    ABIME("Abimé");

    private final String displayValue;

    private Etat(String displayValue) {
        this.displayValue = displayValue;
    }

    public String getDisplayValue() {
        return displayValue;
    }
}
