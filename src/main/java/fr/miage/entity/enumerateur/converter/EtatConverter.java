package fr.miage.entity.enumerateur.converter;

import fr.miage.entity.enumerateur.Etat;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.stream.Stream;

@Converter(autoApply = true)
public class EtatConverter implements AttributeConverter<Etat, String> {

    @Override
    public String convertToDatabaseColumn(Etat attribute) {
        if(attribute == null)
            return null;
        else
            return attribute.getDisplayValue();
    }

    @Override
    public Etat convertToEntityAttribute(String dbData) {
        if (dbData == null)
            return null;
        return Stream.of(Etat.values())
                .filter(c -> c.getDisplayValue().equals(dbData))
                .findFirst().get();
    }
}
