package fr.miage.resources;

import fr.miage.entity.livreusager.Oeuvre;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OeuvreResource extends JpaRepository<Oeuvre, String> {

}
