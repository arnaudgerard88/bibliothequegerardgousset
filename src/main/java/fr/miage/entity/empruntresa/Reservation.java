package fr.miage.entity.empruntresa;

import fr.miage.entity.livreusager.Oeuvre;
import fr.miage.entity.livreusager.Usager;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Reservation {

    @Id
    String id;
    Date datejour;
    @ManyToOne
    @JoinColumn(name = "id_oeuvre")
    Oeuvre oeuvre;
    @ManyToOne
    @JoinColumn(name = "id_usager")
    Usager usager;
    public static final int NB_JOURS_AVANT_RETOUR = 7;

    public Reservation() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getDatejour() {
        return datejour;
    }
    
	public void setDatejour(Date datejour) {
        this.datejour = datejour;
    }

    public Oeuvre getOeuvre() {
        return oeuvre;
    }

    public void setOeuvre(Oeuvre oeuvre) {
        this.oeuvre = oeuvre;
    }

    public Usager getUsager() {
        return usager;
    }

    public void setUsager(Usager usager) {
        this.usager = usager;
    }
}
