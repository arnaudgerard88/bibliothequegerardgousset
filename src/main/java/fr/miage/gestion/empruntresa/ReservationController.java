package fr.miage.gestion.empruntresa;

import fr.miage.entity.empruntresa.Reservation;
import fr.miage.resources.OeuvreResource;
import fr.miage.resources.ReservationResource;
import fr.miage.resources.UsagerResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.UUID;

@Controller
@RequestMapping("/reservation")
public class ReservationController {

    @Autowired
    ReservationResource rr;

    @Autowired
    OeuvreResource or;

    @Autowired
    UsagerResource ur;

    @GetMapping("/list")
    public String list(Model model) {
        model.addAttribute("module", "reservation");
        model.addAttribute("reservations", rr.findAll());
        return "reservation/listereservation";
    }

    @GetMapping("/create")
    public String create(Model model) {
        model.addAttribute("module", "reservation");
        model.addAttribute("reservation", new Reservation());
        model.addAttribute("oeuvres", or.findAll());
        model.addAttribute("usagers", ur.findAll());
        return "reservation/creerreservation";
    }

    @PostMapping("/create")
    public String createPost(@Valid @ModelAttribute Reservation reservation, BindingResult result) {
        if(result.hasErrors()) {

        } else {
            reservation.setId(UUID.randomUUID().toString());
            reservation.setDatejour(new Date());
            //reservation.setDateRetour(new Date(new Date().getTime()+Reservation.NB_JOURS_AVANT_RETOUR*24*60*60*1000));
            rr.save(reservation);
        }
        return "redirect:/reservation/list";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable String id) {
        Reservation reservation = rr.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Id reservation non valide: "+ id));
        rr.delete(reservation);
        return "redirect:/reservation/list";
    }


}
