package fr.miage.entity.enumerateur.converter;


import fr.miage.entity.enumerateur.Type;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.stream.Stream;

@Converter(autoApply = true)
public class TypeConverter implements AttributeConverter<Type, String> {

    @Override
    public String convertToDatabaseColumn(Type attribute) {
        if(attribute == null)
            return null;
        else
            return attribute.getDisplayValue();
    }

    @Override
    public Type convertToEntityAttribute(String dbData) {
        if (dbData == null)
            return null;

        return Stream.of(Type.values())
                .filter(c -> c.getDisplayValue().toUpperCase().equals(dbData.toUpperCase()))
                .findFirst().get();
    }
}

