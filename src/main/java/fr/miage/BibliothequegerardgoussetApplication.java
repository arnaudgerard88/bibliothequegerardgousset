package fr.miage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BibliothequegerardgoussetApplication {

	public static void main(String[] args) {
		SpringApplication.run(BibliothequegerardgoussetApplication.class, args);
	}

}