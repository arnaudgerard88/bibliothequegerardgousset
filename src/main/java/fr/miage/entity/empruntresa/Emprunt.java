package fr.miage.entity.empruntresa;

import fr.miage.entity.livreusager.Exemplaire;
import fr.miage.entity.livreusager.Usager;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.util.Date;

@Entity
public class Emprunt {

    @Id
    String id;
    Date datejour;
    Date dateretour;
    @ManyToOne
    @JoinColumn(name = "id_usager")
    Usager usager;
    @ManyToOne
    @JoinColumn(name = "id_exemplaire")
    Exemplaire exemplaire;

    public Emprunt() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getDatejour() {
        return datejour;
    }

    public void setDatejour(Date datejour) {
        this.datejour = datejour;
    }

    public Date getDateretour() {
        return dateretour;
    }

    public void setDateretour(Date dateretour) {
        this.dateretour = dateretour;
    }

    public Usager getUsager() {
        return usager;
    }

    public void setUsager(Usager usager) {
        this.usager = usager;
    }

    public Exemplaire getExemplaire() {
        return exemplaire;
    }

    public void setExemplaire(Exemplaire exemplaire) {
        this.exemplaire = exemplaire;
    }
}
