package fr.miage.gestion.livreusager;

import fr.miage.entity.livreusager.Auteur;
import fr.miage.resources.AuteurResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;

@Controller
@RequestMapping("auteur")
public class AuteurController {

    @Autowired
    AuteurResource ar;

    @GetMapping("/list")
    public String list(Model model) {
        model.addAttribute("module", "auteur");
        model.addAttribute("auteurs", ar.findAll());
        return "auteur/listeauteur";
    }

    @GetMapping("/create")
    public String create(Model model) {
        model.addAttribute("module", "auteur");
        model.addAttribute("auteur", new Auteur());
        return "auteur/creerauteur";
    }

    @PostMapping("/create")
    public String createPost(@Valid @ModelAttribute Auteur auteur, BindingResult result) {
        auteur.setId(UUID.randomUUID().toString());
        ar.save(auteur);
        return "redirect:/auteur/list";
    }
}
