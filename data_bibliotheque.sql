CREATE EXTENSION "uuid-ossp";
INSERT INTO public.usager(id, nom, prenom, email, telephone) VALUES (uuid_generate_v4(), 'Gousset', 'Yann', 'Gousset.y@mail.fr', '0667676767');
INSERT INTO public.usager(id, nom, prenom, email, telephone) VALUES (uuid_generate_v4(), 'Gerard', 'Arnaud', 'Gerard.a@mail.fr', '0667676768');
INSERT INTO public.usager(id, nom, prenom, email, telephone) VALUES (uuid_generate_v4(), 'Jean', 'Paul', 'Jean.p@mail.fr', '0667676766');

INSERT INTO public.auteur(id, nom, prenom) VALUES (uuid_generate_v4(), 'Rowling', 'J. K.');
INSERT INTO public.oeuvre(id, titre, stock, type) VALUES (uuid_generate_v4(), 'Harry Potter', 1, 'livre');
INSERT INTO public.livreauteur(id, id_auteur, id_oeuvre) VALUES (uuid_generate_v4(), (select a.id from auteur a where nom like 'Rowling'), (select o.id from oeuvre o where o.titre like 'Harry Potter'));

INSERT INTO public.oeuvre(id, titre, stock, type) VALUES (uuid_generate_v4(), 'Picsou magazine', 0, 'magazine');

INSERT INTO public.exemplaire(id, etat, dispo, id_oeuvre) VALUES (uuid_generate_v4(), 'Neuf', false, (select o.id from oeuvre o where titre like 'Harry Potter'));

INSERT INTO public.reservation(id, datejour, id_oeuvre, id_usager) VALUES (uuid_generate_v4(), current_date, (select o.id from oeuvre o where titre like 'Harry Potter'), (select u.id from usager u where nom like 'Gerard'));

INSERT INTO public.emprunt(id, datejour, dateretour, id_usager, id_exemplaire) VALUES (uuid_generate_v4(), current_date, current_date + 7, (select u.id from public.usager u where u.nom like 'Gousset'), (select e.id from oeuvre o inner join exemplaire e on o.id = e.id_oeuvre where titre like 'Harry Potter'))
