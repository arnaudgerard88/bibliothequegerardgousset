package fr.miage.resources;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.miage.entity.livreusager.Usager;
import org.springframework.data.jpa.repository.Query;

public interface UsagerResource extends JpaRepository<Usager, String> {


}
