package fr.miage.resources;

import fr.miage.entity.livreusager.Livre;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LivreResource extends JpaRepository<Livre, String> {
}
