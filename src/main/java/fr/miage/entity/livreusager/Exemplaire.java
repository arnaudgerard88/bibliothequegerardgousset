package fr.miage.entity.livreusager;

import fr.miage.entity.enumerateur.Etat;

import javax.persistence.*;

@Entity
public class Exemplaire {

    @Id
    private String id;
    private Etat etat;
    private boolean dispo;
    @ManyToOne()
    @JoinColumn(name = "id_oeuvre")
    private Oeuvre oeuvre;

    public Exemplaire() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Etat getEtat() {
        return etat;
    }

    public void setEtat(Etat etat) {
        this.etat = etat;
    }

    public boolean isDispo() {
        return dispo;
    }

    public void setDispo(boolean dispo) {
        this.dispo = dispo;
    }

    public Oeuvre getOeuvre() {
        return oeuvre;
    }

    public void setOeuvre(Oeuvre oeuvre) {
        this.oeuvre = oeuvre;
    }

    @Override
    public String toString() {
        return "Exemplaire{" +
                "id='" + id + '\'' +
                ", etat=" + etat +
                ", dispo=" + dispo +
                ", oeuvre=" + oeuvre +
                '}';
    }
}
