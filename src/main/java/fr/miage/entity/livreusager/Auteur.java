package fr.miage.entity.livreusager;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Auteur {
    @Id
    private String id;
    private String nom;
    private String prenom;
    @OneToMany(mappedBy = "auteur")
    private Set<Livreauteur> livres;

    public Auteur() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Set<Livreauteur> getLivres() {
        return livres;
    }

    public void setLivres(Set<Livreauteur> livres) {
        this.livres = livres;
    }
}
