package fr.miage.gestion.livreusager;

import fr.miage.entity.livreusager.Usager;
import fr.miage.resources.EmpruntResource;
import fr.miage.resources.ReservationResource;
import fr.miage.resources.UsagerResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;

@Controller
@RequestMapping("/usager")
public class UsagerController {
    @Autowired
    private UsagerResource ur;

    @Autowired
    private EmpruntResource emr;

    @Autowired
    private ReservationResource rr;

    @GetMapping("/list")
    public String list(Model model) {
        model.addAttribute("usagers", ur.findAll());
        model.addAttribute("module", "usager");
        return "usager/listeusagers";
    }

    @GetMapping("/create")
    public String creer(Model model) {
        model.addAttribute("usager", new Usager());
        model.addAttribute("module", "usager");
        return "usager/creerusager";
    }

    @PostMapping("/create")
    public String creerPost(@ModelAttribute Usager usager) {
        usager.setId(UUID.randomUUID().toString());
        Usager saved = ur.save(usager);
        return "redirect:/usager/list";
    }

    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, Model model) {
        Usager usager = ur.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Id usager non valide:" + id));
        model.addAttribute("usager", usager);
        model.addAttribute("module", "usager");
        return "usager/modifierusager";
    }

    @PostMapping("/edit/{id}")
    public String editPost(@PathVariable("id") String id, @Valid @ModelAttribute Usager usager, BindingResult result) {
        usager.setId(id);
        ur.save(usager);
        return "redirect:/usager/list";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") String id, Model model) {
        Usager usager = ur.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Id usager non valide:" + id));
        if(!emr.findEmpruntByUsager(usager).isEmpty()) {
            return "usager/erreurusager";
        }
        if(!rr.findReservationByUsager(usager).isEmpty()) {
            return "usager/erreurusager";
        }
        ur.delete(usager);
        return "redirect:/usager/list";
    }
}
