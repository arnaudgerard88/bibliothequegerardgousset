# Bibal

Une application de gestion des emprunts et des réservations sur les œuvres d'une bibliothèque

## Getting Started

### Installation

Dans un premier temps il faut créer une nouvelle database bibliotheque.
```
CREATE DATABASE bibliotheque
```
Ensuite il faut importer la structure à partir du fichier

```
database-bibliotheque.sql
```

Et on pour tester on peut ajouter les données à partir du fichier
 ```
data_bibliotheque.sql
```

Faire un maven package pour récuperer les dépendances maven
```
mvn clean package -DskipTests
```

### Execution

Pour lancer le projet il faut executer le main de la classe BibliothequegerardgoussetApplication
ou avec la commande maven suivante
```
mvn spring-boot:run
```

L'application sera accessible à partir de l'url en local sur le port 9090 par défaut
```
localhost:9090
```
## Outils

* [Maven](https://maven.apache.org/) - Gestion de dépendances
* [Spring](https://spring.io/) - Framework
* [Thymeleaf](https://www.thymeleaf.org/) - Moteur de template
* [Postgresql](https://www.postgresql.org/) - SGBD

## Auteurs

* **GOUSSET Yann**
* **GERARD Arnaud**