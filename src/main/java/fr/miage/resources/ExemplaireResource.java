package fr.miage.resources;

import fr.miage.entity.livreusager.Exemplaire;
import fr.miage.entity.livreusager.Oeuvre;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ExemplaireResource extends JpaRepository<Exemplaire, String> {

    @Query("select e from Exemplaire e where e.oeuvre = ?1 and e.dispo = true")
    List<Exemplaire> findExemplaireDispoByIdOeuvre(Oeuvre oeuvre);

    @Query("select e from Exemplaire e where e.dispo = true")
    List<Exemplaire> findExemplaireDispo();
}
