CREATE TABLE public.usager
(
    id text,
    nom text,
    prenom text,
    email text,
    telephone text,
    PRIMARY KEY (id)
);
ALTER TABLE public.usager
    OWNER to yann;

CREATE TABLE public.oeuvre
(
    id text,
    titre text,
    stock integer,
    type text,
    PRIMARY KEY (id)
);
ALTER TABLE public.oeuvre
    OWNER to yann;

CREATE TABLE public.auteur
(
    id text,
    nom text,
    prenom text,
    primary key(id)
);
ALTER TABLE public.auteur
    owner to yann;

CREATE TABLE public.livreauteur
(
    id text,
    id_auteur text,
    id_oeuvre text,
    CONSTRAINT livreauteur_auteur_fk FOREIGN KEY (id_auteur)
        REFERENCES public.auteur (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT livreauteur_livre_fk FOREIGN KEY (id_oeuvre)
        REFERENCES public.oeuvre (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    primary key (id)
);
ALTER TABLE public.livreauteur
    owner to yann;

CREATE TABLE public.exemplaire
(
    id text,
    etat text,
    dispo boolean,
    id_oeuvre text,
    PRIMARY KEY (id),
    CONSTRAINT exempl_oeuvre_fkey FOREIGN KEY (id_oeuvre)
        REFERENCES public.oeuvre (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);
ALTER TABLE public.exemplaire
    OWNER to yann;

CREATE TABLE public.reservation
(
    id text,
    datejour date,
    id_oeuvre text,
    id_usager text,
    PRIMARY KEY (id),
    CONSTRAINT resa_oeuvre_fkey FOREIGN KEY (id_oeuvre)
        REFERENCES public.oeuvre (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT resa_usager_fkey FOREIGN KEY (id_usager)
        REFERENCES public.usager (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

ALTER TABLE public.reservation
    OWNER to yann;

CREATE TABLE public.emprunt
(
    id text,
    datejour date,
    dateretour date,
    id_usager text,
    id_exemplaire text,
    PRIMARY KEY (id),
    CONSTRAINT empr_usager_fk FOREIGN KEY (id_usager)
        REFERENCES public.usager (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT empr_exempl_fk FOREIGN KEY (id_exemplaire)
        REFERENCES public.exemplaire (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

ALTER TABLE public.emprunt
    OWNER to yann;
