package fr.miage.entity.enumerateur;

public enum Type {
    MAGAZINE("Magazine"),
    LIVRE("Livre");

    private final String displayValue;

    private Type(String displayValue) {
        this.displayValue = displayValue;
    }

    public String getDisplayValue() {
        return displayValue;
    }
}